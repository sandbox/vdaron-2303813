= AsciiDoctor filter =

This module provides AsciiDoc filter integration for Drupal input
formats using asciidoctor engine.

== Features ==

* Input filter to generate XHTML based on AsciiDoc syntax.

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/asciidocdoc

== Requirements ==

* External packages:
** Asciidoctor: http://asciidoctor.org/
** Tilt : http://TODO (only required if custom theme are specified)

== Instalation ==

* Install as usual, see http://drupal.org/node/xxxx for further
information.

== Configuration ==

. Set up a new input format or add Asciidoctor support to an existing format at
   Administer >> Site configuration >> Input formats

. For best security, ensure that:
  .. The HTML filter is after the Markdown filter on the "Reorder" page of
     the input format.
  .. Only markup you would like to allow in via AsciiDoc is configured to
     be allowed via the HTML filter.

. Three parameters are available for this module
  
  .. $safe_mode : Set safe mode level: unsafe, safe, server or secure (default to secure). Do NOT use 'unsafe' unless you really know what you are doing !

  .. $iconsdir : Path where admonition icons can be found (note.png,...)

  .. $templatedir : Path where custom erb theme can be found to customize the html generation. If used 'tilt' gem must be installed using "gem install tilt" command

== Credits ==

 * The authors of Asciidoctor.
 * Largely inspired by "Asciidoc filter" from Marco Villegas (marvil07)

Current Maintainer: Vincent DARON (vdaron)
